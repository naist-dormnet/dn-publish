利用ガイドは2015年度より、Web画面との連携を円滑にするために、HTMLベースの編集をしています。
その際、より容易にHTMLを生成するために、[Middleman](https://middlemanapp.com/)を使用しています。

システム要件
------------

* Ruby +2.0.0
* mplus-1p
* Git

編集の流れ
----------

編集の流れを次に示します。

1. プロジェクトの最新版をリモートリポジトリからpull
2. 新しいブランチをmasterから作成
3. ファイルを編集して、変更をコミット
4. 変更をリモートリポジトリにpush
5. 文章のレビューをする
6. 大丈夫そうなら、変更をmasterにマージ
7. masterブランチを本番環境にデプロイ

### 環境をインストール

まず、お手持ちの環境にbundleが入ってるか確認します。

```sh
bundle -v
```

もし入ってなければ、bundleを手元の環境にインストールします。

```sh
setenv GEM_HOME ~/.gem/ruby/2.0.0
gem install bundler                   # Install bundler
```

そして `~/.cshrc` にバンドルコマンドにパスを通す設定を追加します。

```sh
setenv PATH  ~/.gem/ruby/2.0.0/bin:"$PATH"
```

最ログイン後、bundleが使えるはずです。

```
bundle -v
```

### プロジェクトの更新

dn-publishプロジェクトを手元にcloneします。

```sh
git clone git@dn-talk.naist.jp:dormnet/dn-publish.git
cd dn-publish
```

すでにプロジェクトが手元にある場合、masterの最新版をリモートリポジトリから取得します。

```sh
cd dn-publish 	# Enter the project directory
git checkout master
git pull origin master
```

つぎに、Middlemanとその依存関係をインストールします。

```sh
bundle install --path vendor/bundle
```

これでMiddlemanのプレビューサーバーが起動するはずです。

```sh
bundle exec middleman server
```

http://localhost:4567/ を開くことで、HTMLファイルをプレビューすることができます。

### ページの編集

ページを編集する前に、新たなブランチを作成します。
Before you modify the file, create a branch in the git local repository.

```sh
git checkout -b <the_name_of_purpose_or_change> origin/maste
```

そしてファイルの編集とプレビューを繰り返しおこないます。
編集がおわったら、ファイルの変更点を見ます．

```sh
git status  # prints current status
git diff    # prints differences between last commit
```

変更が良さそうだったら、ファイルを追加してコミットします。

```sh
git add file1 file2 ...
git commit
```

最後に、ページをpushします。

```
git push origin <the_name_of_purpose_or_change>
```

### 作成したブランチをmasterにマージ

現時点でまだ変更はmasterにマージされていません。
masterにマージするために [Merge Request](https://dn-talk.naist.jp/gitlab/dormnet/dn-publish/merge_requests) を作成します。
Merge Request を作成するとファイルの差分が見えるので、他の人にレビューしてもらいましょう。
レビューしてもらってOKなら、mergeボタンを押してmasterにマージします。

### 最新の変更をデブロイ

masterにマージ後、デプロイすることで本番環境のHTMLも更新されます。
まず手元のプロジェクトのブランチをmasterに切り替え、最新版を取得します。

```sh
git checkout master
git pull origin master
git log   # It should contain your changes
```

そして最新の変更をデプロイします。

```sh
bundle exec middleman deploy
```

### PDF出力

ブラウザで `http://localhost:4567/` を開き、所望のページを開いてPDFに出力するだけで、キレイにファイルが出力されるはずです。

ファイル階層
------------
* `source/`
  * `en/` 利用ガイド(en) 内容
  * `ja/` 利用ガイド(ja) 内容
  * `guidebook_en.slim` 利用ガイド(en)のindexファイル
  * `guidebook_en.slim` 利用ガイド(ja)のindexファイル
  * `schedule.slim` ガイダンススケジュール
