<h1 class='cover-title'>Dormitory Network Users’ Guide</h1>
<p class='to'>To all residents</p>
<p class='from'>
  Information initiative center<br>
  Student Support Section, Educational Affairs Division<br>
  Dormitory network committee<br>
  dormnet-staff@itc.naist.jp
</p>
<p class='date'> Revised in March 2017</p>


<p class='notice'>
This guide contains all the necessary procedures for connection and some
other important information.  Please read this document before connecting
to the dormitory network.
</p>


<h2 class='nonumber'>Introduction</h2>

All users of the dormitory network must adhere to the **"Regulations for Network Usage in Dormitories"** at all times when connecting any personal electronic devices to the dormitory network. Please read the Regulations thoroughly before providing your consent.
