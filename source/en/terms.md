Notes while using the Dormitory Network
=======================================

Don't forget security issues and threats of viruses
---------------------------------------------------

All users must take measure against security issues and viruses.
Institutional authorities may take some actions on irresponsible.  In past
times, some users without measures were punished, and they were prohibited
from using the dormitory network.  This network is constructed inside the
MANDARA network's firewall.  However, this firewall is not effective
against attacks from inside the Dormitory network or MANDARA network.
Therefore, all users must take measures against by enabling firewall on
your OS.

You must also take measures against viruses.  Through the network, some
viruses increase or send abnormal traffic.  Users must always do the
following:

1. Always use the latest version of the OS.
2. Install Anti-Virus software.
3. Keep Anti-Virus definition file updated.

Regarding anti-virus software, you can use Microsoft Security Essentials
if you use Windows.  You can also get campus license software provided by
ITC.  These days, viruses are not only for Windows but also Unix-based OS.
All users must take measures against viruses.

* Microsoft Security Essentials<br>
  <cite>http://windows.microsoft.com/en-us/windows/security-essentials-download</cite>
* Campus License Software Distribution Server
     <cite>https://naist-ld.naist.jp/</cite>


Service restriction on dormitory network
----------------------------------------

Some services are restricted on the Dormitory network because of technical
and security issues.  General services (Web, e-mail, messenger, Skype,
games etc.) are available.  However, file sharing softwares with P2P is
forbidden.  For more information, please see ITC's web page "Use of P2P
Software"
(<cite>http://itcw3.naist.jp/ITC-local/policy/p2p/index.en.html</cite>).
In addition, you can't set any servers for hosts outside the campus.

Other important notices
-----------------------

### Please be considerate about the network traffic.

All connections from residences are terminated by the same router.  If
someone sends large data, the whole dormitory network will halt.  Also, if
the problems affect the MANDARA network because of the traffic, ITC will
disconnect the dormitory network from it.  These troubles are avoidable if
users pay attention to the recommendations.

### Dormitory network committee makes a log

The Dormitory network committee records the communication logs when a
trouble occurs.  It contains information like when a user used the
network.  This information is handled under the user’s agreement and with
the most possible care.  If a member of the committee makes abuse of this
information, he/she will assume the responsibility of his/her actions.

### Please avoid any inappropriate communication for a national university.

All equipment of the Dormitory network is national property like the
MANDARA network.  Outsiders think that all communication is sent from
NAIST.  Therefore, you should be able to define your actions as proper or
improper for a national university.  Some types of communications need to
be taken into account: those that are offensive against public order and
do no maintain a standard of decency, publicity of politics and religion.
In the same way, you should avoid commercial activities.

### You have the responsibility for all the equipment in your room

The information socket on the wall divides this responsibility, our
institution manages physical connections from server to socket, and the
user has the responsibility for not only hardware but software as well.
Therefore, please avoid asking workers of the university about your
responsibility area.
