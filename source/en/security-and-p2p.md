<h1 class='caution-title'>Notices for wholesome Internet</h1>

Please take actions against virus and illegal access
----------------------------------------------------

We recommend that all users take actions against virus and illegal access.
Some troubles such as

* virus infection on PC
* virus infection by exploiting a security hole over Dormitory Network

had occurred.
When a trouble happens, we have to halt entire dormitory network.
As result, one user cause trouble to everyone.
If the equipment of cause was not taken any measures, its owner may be
punished by our institute, such as disconnection from dormitory network.
Main measurements are

* install and updating Anti-Virus software
* update your OS.

Please refer "Computer Security on MANDARA" to take security measurements.  
 [http://itcw3.naist.jp/ITC-local/policy/security/index.en.html](http://itcw3.naist.jp/ITC-local/policy/security/index.en.html)

Prohibit connection from Windows XP
-----------------------------------

According to the Microsoft's announcement, support for Windows XP have
ended on Apr. 9, 2014.  There will be no more security updates or
technical support for the Windows XP operating system.  We will consider
discontinuation from network when we detect the use of Windows XP or any OS unsupported.

P2P file sharing software
-------------------------

ITC prohibits the use of P2P file sharing software, the dormitory is not
an exception.  Some violators have been penalized before.  Softwares using
P2P technology in background are also subjects.  Softwares to watch TV on
internet have possibilities containing P2P.  Please check the official
site.

Examples of P2P file sharing softwares :
<p class='example-list'>
BitTorrent, μTorrent, BitComet, BitTorrent DNA, Xunlei(迅雷), eDonkey,
WinMX, GNUTella, LimeWire, Cabos, Share, Kazaa, Winny, Azureus, ppstream,
Veoh
</p>
