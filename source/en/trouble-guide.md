Inquiries on troubles
=====================
 
We investigate causes when your personal computer is not connected with
our network. Please send us e-mail at dormnet-staff@itc.naist.jp with
information as follows:

* Your name, room number, MANDARA account, and e-mail address
* Your machine's spec, OS etc.
* Data when problem arise.
* Details e.g. error message and situation to arise
* Result of commands on command prompt.

#### Windows :

```
ipconfig /all
arp -a
route PRINT
ping 163.221.156.1
nslookup dn-sluice.naist.jp
```

#### Mac OS X, or Unix-based OS :

```
ifconﬁg -a
arp -a
netstat -rn
ping -c4 163.221.156.1
nslookup dn-sluice.naist.jp
```

Please understand that we cannot guarantee prompt response.
