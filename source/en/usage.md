Procedure to use dormitory network
==================================

Procedure for connection
------------------------

It is required to assist to the guidance lecture, and after that,
apply to use internet in your room.

### The guidance

The guidances are held one or more on a month.
The schedule of the guidances can be found in the dormitory network
committee guidance information, which is distributed by the student
affairs division.  After the guidance, you can fill the application at the
time you connect to the Internet in your room.

### Requirements for the application

In the dormitory, there is only the wired Ethernet service.  We can
identify you by your MANDARA account in order to confirm that you are
resident.  You cannot connect to the dormitory network without the
account.

### Online application

After the guidance, you can input your information on the application
form.  Please check that your PC is set to receive an IP address
automatically with DHCP before connecting it to the outlet on the wall.

What you can see on the screen when you access
<cite>https://dn-sluice.naist.jp/</cite> is shown in Fig. 1.
Then input your MANDARA account name and password.

![](login.en.png)
<p class='caption'>Application page</p>

After inputting your MANDARA information, the page shown in Fig. 2, will
appear.  If you agree the Regulations for Network Usage in Dormitories, please check a checkbox and click
on "Register".  Then, after restarting your computer, the global IP address
is allocated and you can start using the network.

![](registration.en.png)
<p class='caption'>Application form</p>

When a message like Fig. 3 appears, it means that you have to meet the
requirements or you are banned because of infraction.  If you see this page
though you don’t receive a suspended, please inquire to the dormitory
network committee.

![](banned.en.png)
<p class='caption'>Application error</p>

FAQ and remarks
---------------

### The alert about digital certificate appears

During the application procedure, when a message about the digital
certificate appears, click "continue", or click "OK" on the display of the
security error.  These messages happen because the certificate is
effective only inside the MANDARA network.After completing all
procedures for connecting to the network, please see ITC's web page "about CA(Japanese only)"
and install certificates.
(<cite>http://itcw3.naist.jp/ITC-local/Security/aboutCACertificat.html</cite>)

### Application form does not appear

If you cannot open the application form, please try reboot your PC several
times after a while If the application form does not show at all, please
inquire the dormitory network committee.

### Remarks about IP address

We gives only one fixed IP Address and Host Name.  Use a router if you
need to connect multiple devices.  You can also use your own Wi-fi AP but
you have to activate the security by encryption of the wireless network.
Although some rooms have two connectors, you cannot get two IPs, and
cannot use the two connectors at the same time.



Reference informations
----------------------

The following informations are usually set automatically by DHCP.
Please refer to them when you need them.

* Before registration

<dl>
  <dt>IP address and netmask</dt>
  <dd>10.254.252.0/22</dd>
  <dt>Default gateway</dt>
  <dd>10.254.252.1</dd>
  <dt>DNS servers</dt>
  <dd>163.221.8.11, 163.221.8.12</dd>
</dl>


* After registration

<dl>
  <dt>IP address and netmask</dt>
  <dd>163.221.X.Y/22</dd>
  <dt>Host name</dt>
  <dd>dn-X-Y.naist.jp</dd>
  <dt>default gateway</dt>
  <dd>163.221.156.1</dd>
  <dt>DNS servers</dt>
  <dd>163.221.8.11, 163.221.8.12</dd>
</dl>
