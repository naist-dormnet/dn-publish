利用中の注意事項
================

ウイルスや不正アクセスへの対策の徹底をお願いします
--------------------------------------------------

利用者の皆さんは、ウイルスへの対策をとって下さい。
過去にも、

* ネットワーク利用中にBlasterやNETSKY等のウイルスに一斉感染するケース
* ネットワークを介して伝染するトロイの木馬により、対策を行っていないPCが知らないうちに相次いで感染するケース

がありました。
対策をとっていない利用者が原因の場合には、大学が当該利用者の接続を停止したり、
研究室の教授と連名での始末書を提出させるなどの処分を行った事例があります。
この場合、研究室にも迷惑をかけることになりますので、対策をお願いします。

学生宿舎ネットワークは、曼陀羅ネットワークのファイアウォールの中に構築されています。
しかし、学生宿舎ネットワークや曼陀羅ネットワークの中で攻撃が発生した場合にはこのファイアウォールは役に立ちません。
従って、各利用者のOSのファイアーウォールを有効にするなど、対策をしてください。

同様に、不正アクセスにも対策をお願いします。
一部のウイルスや悪意を持った攻撃者はOSのセキュリティホールを介して知らない間に利用者の機器を乗っ取り、さらに大きな不正アクセスの踏み台に使われることがあります。

対策として以下の3つのことを行ってください。

1. 常に最新のOSを用いること
2. ウイルス対策ソフトを導入すること
3. ウイルス対策ソフトを最新の状態に保つこと

ウイルス対策ソフトについて、Windowを利用している場合、Microsoft社から無償で提供されている
Microsoft Security Essentials が利用できます。
またITCからもいくつかのウィルス対策ソフトが提供されています。
詳しくは以下のURLを参照ください。
最近はUnixやMacなど、Windows以外のOSにもウイルスが登場しています。
全ての利用者には対策をとっていただくようお願いします。

* Microsoft Security Essentials <cite>http://windows.microsoft.com/ja-jp/windows/security-essentials-download</cite>
* キャンパスライセンスウェア配布サーバ <cite>https://naist-ld.naist.jp/</cite>

使用できるサービスには制約があります
------------------------------------

学生宿舎ネットワークでは、技術・セキュリティ上の理由から使えるサービスを限定しています。
一般的なサービス（Web、e-mail、メッセンジャー、Skypeやゲーム等）は使うことが出来ます。
ただし、P2P技術を用いたファイル交換（Bittorrent含む）は大学の方針により禁止されています。
詳細はITCのWebページ「P2Pソフトウェア利用に関して」
(<cite>http://itcw3.naist.jp/ITC-local/policy/p2p/p2p-request.html</cite>)をご覧ください。
また、学外向けサーバを設置することは出来ません。

その他の注意事項
----------------

### ネットワークの利用状況に関心を持ってください

学生宿舎ネットワークは全て一つのルータに接続されています。
従って、特定の利用者が大量に通信を行うと学生宿舎ネットワーク全体が停止する可能性があります。
また、曼陀羅ネットワークの管理上問題がある場合には、ITCが学生宿舎
ネットワーク全体を遮断する場合があります。
このような事態にならないよう、学生宿舎ネットワークの利用状況に関心を持っていただくようお願いします。

### 委員会では通信記録を作成しています

通常、委員会では通信記録は作成していませんが、トラブルが生じた時は通信記録を作成することがあります。
これは誰がいつ通信を行ったかという情報です
（具体的な項目はセキュリティ上の理由により公表できません）。
通信記録は利用条件に基づいて管理しており、委員会が乱用した場合には大学規則や法令に処罰されます。

### 公序良俗に反する通信や商業、政治・宗教活動は避けてください

曼陀羅ネットワークの機器と同様に学生宿舎ネットワークの機器は国有財産です。
学生宿舎ネットワークからの通信は対外的には大学の行った通信です。
公序良俗に反する通信や政治・宗教活動に関する公的な情報発信などの、
国立大学としてふさわしくない通信は避けるようにお願いします。
同様に学生宿舎ネットワークを利用して商業活動を行うことは避けてください。

### 壁の情報コンセントから先の管理は自己責任です

学生宿舎ネットワークの管理は壁の情報コンセントを境にITCと各居住者に分かれます。
計算機室から情報コンセントまでの物理的な部分についてはITCが管理責任を持ちます。
この部分の保守はITCが担当します。
一方、情報コンセントから先にある各個人の計算機等はソフトウェアも含めて自己責任で管理していただきます。
従って、自己責任で管理する部分に関してはITCや委員会への問い合わせはご遠慮ください。
