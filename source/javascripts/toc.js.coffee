initializeToc = ->
  toc = document.getElementById('toc')
  for e in document.getElementsByTagName('h1')
    continue unless e.id
    url = e.id
    text = e.innerHTML
    toc.innerHTML += "<li><a href='##{url}'>#{text}</a></li>"

window.onload = ->
  initializeToc()
